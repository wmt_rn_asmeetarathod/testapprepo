/* eslint-disable  */
import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';

import Routes from 'src/router/Routes';

// Screens Name
import Home from 'src/screens/Authenticated/HomeScreen';
import UserListScreen from 'src/screens/Authenticated/UserListScreen';
import UserDetailsScreen from 'src/screens/Authenticated/UserDetailsScreen';
import AddArticleScreen from 'src/screens/Authenticated/AddArticleScreen';
import DisplayImageScreen from 'src/screens/Authenticated/DisplayImageScreen';
import DisplayMapScreen from 'src/screens/Authenticated/DisplayMapScreen';
import DrawRouteMapScreen from '../screens/Authenticated/DrawRouteMapScreen';
import { Label, Ripple } from 'src/component';
import Maps from '../screens/Authenticated/DrawRouteMapScreen/maps';

const Stack = createStackNavigator();

const navigator = () => {
    return (
        <Stack.Navigator
            initialRouteName={__DEV__ ? Routes.Home : Routes.Home}
            screenOptions={({navigation}) => ({
                headerTitleAlign: 'center',
            })}>
            <Stack.Screen
                name={Routes.Home}
                component={Home}
                options={{headerShown: false}}
            />
            <Stack.Screen
                name={Routes.UserListScreen}
                component={UserListScreen}
                options={{title: 'User List'}}
            />
            <Stack.Screen
                name={Routes.UserDetailsScreen}
                component={UserDetailsScreen}
                options={{title: 'User Details'}}
            />
            <Stack.Screen
                name={Routes.AddArticleScreen}
                component={AddArticleScreen}
                options={{title: 'Add Article'}}
            />
            <Stack.Screen
                name={Routes.DisplayImageScreen}
                component={DisplayImageScreen}
                options={{title: 'Display Image Screen'}}
            />
            <Stack.Screen
                name={Routes.DisplayMapScreen}
                component={DisplayMapScreen}
                options={{title: 'Display Map Screen'}}
            />
            <Stack.Screen
                name={Routes.DrawRouteMapScreen}
                component={DrawRouteMapScreen}
                options={{title: 'Draw Route Map Screen',
               
            }}
            />
        </Stack.Navigator>
    );
};

export default navigator;
