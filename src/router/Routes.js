/* eslint-disable  */
const Routes = {
    /*  Root Stacks    */

    Authenticated: 'Authenticated',
    UnAuthenticated: 'UnAuthenticated',
    Splash: 'Splash',

    /*  Non-Authenticated Routes    */
    Login: 'Login',

    /*  Authenticated Routes    */

    Home: 'Home',
    UserListScreen : 'UserListScreen',
    UserDetailsScreen : 'UserDetailsScreen',
    AddArticleScreen : 'AddArticleScreen',
    DisplayImageScreen : 'DisplayImageScreen',
    DisplayMapScreen : 'DisplayMapScreen',
    DrawRouteMapScreen : 'DrawRouteMapScreen',

};
export default Routes;
