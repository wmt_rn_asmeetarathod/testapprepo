/* eslint-disable  */
import React, { 
    useState,
    useRef,
} from 'react';

import {
    View,
} from 'react-native';
import { styles } from './styles';
import { CommonStyle, ThemeUtils ,showToast,Constants} from 'src/utils';
import { APIRequest } from 'src/api/api-request';
import {
    MaterialTextInput ,
    RoundButton,
    Label,
} from 'src/component';


const AddArticleScreen = (props) => {

    const [name, setName] = useState();
    const [year, setYear] = useState();
    const photoUrl='https://randomuser.me/api/portraits/women/63.jpg';
    /*  UI Events Methods   */

    const handleAddArticle = () => {
        console.log(" AddArticleScreen ~ name ==>> ", name);
        console.log(" AddArticleScreen ~ year ==>> ", year);
        createArticle();
        setName("");
        setYear("");
        showToast("Article Added Successfully");
    }

    const createArticle = () => {
        new APIRequest.Builder()
            .post()
            .jsonParams(JSON.stringify({name,year}))
            // .addFile('profilePic',photoUrl)
            .reqURL(Constants.AddArticleRequestUrl)
            .response(onResponse)
            .error(onError)
            .build()
            .doRequest();
    }
    const onResponse = (res, reqId) => {
        console.log("onResponse ~ reqId ==>> ", reqId);
        console.log("res ==>> ", res);
    }

    const onError = (e) => {
        console.log("error ==>> ", e);
    }

    return (
        <View style={CommonStyle.master_full_flex}>
            <View style={styles.container}>
                <Label>{'Add Article'}</Label>
                <MaterialTextInput
                    label={'Name'}
                    value={name}
                    onChangeText={(val) => setName(val)}
                />
                <MaterialTextInput
                    label={'Year'}
                    value={year}
                    onChangeText={(val) => setYear(val)}
                />
                <RoundButton
                    click={handleAddArticle}
                    width={ThemeUtils.relativeWidth(90)}
                    mt={ThemeUtils.relativeRealHeight(4)}
                >
                    {'ADD ARTICLE'}
                </RoundButton>
            </View>
        </View>
    );
};

export default AddArticleScreen;
