/* eslint-disable  */
import React, { useState } from 'react';
import Video from 'react-native-video';
import DocumentPicker from 'react-native-document-picker';
import { launchCamera, launchImageLibrary } from 'react-native-image-picker';
import {
    View,
    Image,
    ScrollView,
} from 'react-native';
import { styles } from './styles';
import { Label , RoundButton } from 'src/component';
import { PermissionUtils , ThemeUtils} from 'src/utils';

const DisplayImageScreen = (props) => {

    const [fileUri, setFileUri] = useState('https://randomuser.me/api/portraits/women/79.jpg');

    /*  UI Events Methods   */
    const captureImage = (type) => {

        let options = {
            mediaType: type,
            quality: 1,
            videoQuality: 'low',
            durationLimit: 30, //Video max duration in seconds
            saveToPhotos: true,
        };
        PermissionUtils.requestCameraPermission().then((isGranted) => {
            //it will return true if permission granted otherwise false
            console.log('Camera Permission granted: ', isGranted);
            if (isGranted) {
                launchCamera(options, (response) => {
                    console.log('Response = ', response);

                    if (response.didCancel) {
                        console.log('User cancelled image picker');

                    } else if (response.errorCode == 'camera_unavailable') {
                        console.log('Camera not available on device');

                    } else if (response.errorCode == 'permission') {
                        console.log('Permission not satisfied');

                    } else if (response.errorCode == 'others') {
                        console.log(response.errorMessage);

                    } else {
                        console.log('response ==>> ', JSON.stringify(response));

                        setFileUri(response.uri);
                    }
                });
            }


        });


    }

    const chooseFile = (type) => {
        let options = {
            mediaType: type,
            quality: 1,
        };
        /* For Storage Permission */
        PermissionUtils.requestStoragePermission().then((isGranted) => {
            //it will return true if permission granted otherwise false
            console.log('Storage Permission granted: ', isGranted);
            if (isGranted) {
                launchImageLibrary(options, (response) => {
                    console.log('Response = ', response);

                    if (response.didCancel) {
                        console.log('User cancelled camera picker');
                        setFileUri('https://randomuser.me/api/portraits/women/79.jpg')
                    } else if (response.errorCode == 'camera_unavailable') {
                        console.log('Camera not available on device');
                        setFileUri('https://randomuser.me/api/portraits/women/79.jpg')
                    } else if (response.errorCode == 'permission') {
                        console.log('Permission not satisfied');
                        setFileUri('https://randomuser.me/api/portraits/women/79.jpg')

                    } else if (response.errorCode == 'others') {
                        console.log(response.errorMessage);
                        setFileUri('https://randomuser.me/api/portraits/women/79.jpg')
                    } else{
                        console.log('response ==>>> ', response);
                        setFileUri(response.uri);
                    }

                    
                });
            }
        });


    };

    const onSelectDocument = async () => {

        /* For Storage Permission */
        await PermissionUtils.requestStoragePermission().then((isGranted) => {
            //it will return true if permission granted otherwise false
            console.log('Storage Permission granted: ', isGranted);
            if (isGranted) {
                try {
                    const res = DocumentPicker.pick({
                        type: [DocumentPicker.types.pdf],
                    });
                    console.log('res ==>> ', res);
                } catch (err) {
                    if (DocumentPicker.isCancel(err)) {
                        // User cancelled the picker, exit any dialogs or menus and move on
                        console.log('User cancelled');
                        setFileUri('https://randomuser.me/api/portraits/women/79.jpg')
                    } else {
                        console.log('Error ==>> ', err);
                        throw err;

                    }
                }
            }

        });


    }

    return (
        <ScrollView style={styles.scrollViewStyle}>
            <View style={styles.container}>
            <Label>{'IMAGE PICKER DEMO'}</Label>
            
            { fileUri.toString().endsWith("jpg") ?
                <Image style={styles.imageStyle} source={{ uri: fileUri }} />
                :
                <Video
                    style={styles.videoStyle}
                    source={{ uri: fileUri }}
                    resizeMode='cover'
                    repeat
                />

            }

            <RoundButton
                width={ThemeUtils.relativeWidth(90)}
                click={() => captureImage('photo')}
                mb={20}
            >
                {'Launch Camera For Image'}
            </RoundButton>
            <RoundButton
                width={ThemeUtils.relativeWidth(90)}
                click={() => captureImage('video')}
                mb={20}
            >
                {'Launch Camera For Video'}
            </RoundButton>
            <RoundButton
                width={ThemeUtils.relativeWidth(90)}
                click={() => chooseFile('photo')}
                mb={20}
            >
                {'Choose Image'}
            </RoundButton>
            <RoundButton
                width={ThemeUtils.relativeWidth(90)}
                click={() => chooseFile('video')}
                mb={20}
            >
                {'Choose Video'}
            </RoundButton>
            <RoundButton
                width={ThemeUtils.relativeWidth(90)}
                click={onSelectDocument}
                mb={20}
            >
                {'Select Document'}
            </RoundButton>

        </View>

        </ScrollView>
            );
};

export default DisplayImageScreen;
