/* eslint-disable  */
import { StyleSheet } from 'react-native';
import { Color } from 'src/utils';

export const styles = StyleSheet.create({
    scrollViewStyle:{
        flex:1,
    },
    container: {
        flexGrow: 1,
        alignItems: 'center',
        justifyContent: 'center',
        // backgroundColor: Color.TEXT_SECONDARY,
    },
    imageStyle: {
        height: 200,
        width: 200,
        margin: 20,
        borderRadius: 20,
        backgroundColor: Color.TEXT_SECONDARY,
    },
    videoStyle: {
        
        height: 200,
        width: 200,
        margin: 20,
        borderRadius: 20,
        backgroundColor: Color.TEXT_SECONDARY,
    }
});
