/* eslint-disable  */
import React,
{
    useState,
    useEffect,
    useRef,
} from 'react';
import {
    View,
    Text
} from 'react-native';
import MapViewDirections from 'react-native-maps-directions';
import MapView,
{
    PROVIDER_GOOGLE,
    Marker,
    Polyline,
    Geojson,
} from 'react-native-maps';
import RNLocation from 'react-native-location';
import { styles } from './styles';

const DisplayMapScreen = (props) => {

    const [latitude, setLatitude] = useState(23.0225);
    const [longitude, setLongitude] = useState(72.5714);
    const [destMarker, setDestMarker] = useState('');
    const [startMarker, setStartMarker] = useState('');
    const [markers, setMarkers] = useState(null);
    const [coords, setCoords] = useState();
    let mapRef = useRef(null);

    const origin = '22.9962,72.5996';
    const destination = '23.0134,72.5624';
    const DEFAULT_PADDING = { top: 100, right: 100, bottom: 100, left: 100 };
    // const origin = { latitude: 37.3318456, longitude: -122.0296002 };
    // const destination = { latitude: 37.771707, longitude: -122.4053769 };
    const GOOGLE_MAPS_APIKEY = 'AIzaSyCBPSMNBI2XGaOWjZjEovAIWxmoNBukFLs';

    const [region, setRegion] = useState({
        latitude: 23.0225,
        longitude: 72.5714,
        latitudeDelta: 0.015,
        longitudeDelta: 0.0121,
    });

    useEffect(() => {

        // getRoutePoints(origin, destination);

        RNLocation.configure({
            distanceFilter: 5.0,
        });

        RNLocation.requestPermission({
            ios: "whenInUse",
            android: {
                detail: "coarse"
            }
        }).then(granted => {
            console.log("granted ==>> ", granted);
            if (granted) {

                RNLocation.subscribeToLocationUpdates(locations => {
                    console.log("locations ==>> ", locations);
                    console.log("locations.latitude ==>> ", locations[0].latitude);
                    console.log("locations.longitude ==>> ", locations[0].longitude);

                    setRegion({
                        latitude: locations[0].latitude,
                        longitude: locations[0].longitude,
                        latitudeDelta: 0.015,
                        longitudeDelta: 0.0121,
                    })
                    console.log("region ==>>>>>> ", region);

                    setLatitude(locations[0].latitude);
                    console.log("latitude ==>> ", latitude);

                    setLongitude(locations[0].longitude);
                    console.log("longitude ==>> ", longitude);
                })
            }
        })
            .catch(e => console.log("error ==>>>> ", e));
    }, []);


    const CustomMarker = () => (
        <View
            style={styles.customMarkerStyle}
        >
            <Text style={styles.textStyle}>SVP Hospital</Text>
        </View>
    );


    // This method will give you JSON response with all location points between 2 location

    const getRoutePoints = (origin, destination) => {
        console.log("-----getRoutePoints-----")
        const url = `https://maps.googleapis.com/maps/api/directions/json?origin=${origin}&destination=${destination}&key=${GOOGLE_MAPS_APIKEY}&mode='driving'`;
        console.log("URL == >>", url);

        fetch(url)
            .then(response => response.json())
            .then(responseJson => {
                console.log("responseJson ==>> ", responseJson);
                if (responseJson.routes.length) {
                    let cortemp = decode(responseJson.routes[0].overview_polyline.points) // definition below;
                    let length = cortemp.length - 1;

                    let tempMARKERS = [];
                    tempMARKERS.push(cortemp[0]);   //start origin        
                    tempMARKERS.push(cortemp[length]); //only destination adding

                    console.log("tempMARKERS : " + JSON.stringify(tempMARKERS));
                    setCoords(cortemp);
                    setMarkers(tempMARKERS);
                    setDestMarker(cortemp[length]);
                    setStartMarker(cortemp[0]);

                }
            }).catch(e => { console.warn(e) });
    }


    //  This method will transforms something like this geocFltrhVvDsEtA}ApSsVrDaEvAcBSYOS_@... to an array of coordinates
    const decode = (t, e) => {
        for (var n, o, u = 0, l = 0, r = 0, d = [], h = 0, i = 0, a = null, c = Math.pow(10, e || 5); u < t.length;) {
            a = null, h = 0, i = 0;
            do a = t.charCodeAt(u++) - 63, i |= (31 & a) << h, h += 5; while (a >= 32);
            n = 1 & i ? ~(i >> 1) : i >> 1, h = i = 0;
            do a = t.charCodeAt(u++) - 63, i |= (31 & a) << h, h += 5; while (a >= 32);
            o = 1 & i ? ~(i >> 1) : i >> 1, l += n, r += o, d.push([l / c, r / c])
        }
        return d = d.map(function (t) {
            return {
                latitude: t[0],
                longitude: t[1]
            }
        })
    }


    //   This method will fit all markers point into single screen 

    const fitAllMarkers = () => {
        const temMark = markers;
        console.log("------fitAllMarkers------")

        if (mapRef == null) {
            console.log("map is null")
        } else {
            //option:1  
            console.log("temMark : " + JSON.stringify(temMark));
            mapRef.fitToCoordinates(temMark, {
                edgePadding: DEFAULT_PADDING,
                animated: false,
            });
        }
    }
    const myPlace = {
        type: 'FeatureCollection',
        features: [
            {
                type: 'Feature',
                properties: {},
                geometry: {
                    type: 'Point',
                    coordinates: [23.0259, 72.5658],
                }
            }
        ]
    };
    return (
        <View style={styles.vwMap}>
            <MapView
                ref={ref => { mapRef = ref; }}
                style={styles.mapStyle}
                // showsUserLocation={true}
                showsMyLocationButton={true}
                followsUserLocation={true}
                provider={PROVIDER_GOOGLE}
                pitchEnabled={true}
                loadingEnabled={true}
                initialRegion={region}
                zoomEnabled={true}
                zoomControlEnabled={true}
                toolbarEnabled={true}
                region={region}
                onRegionChange={val => setRegion(val)}
            >

                <Marker
                    coordinate={{ latitude: 23.0225, longitude: 72.5714 }}
                    title={"MyPlace"}
                    description={"My Place"}
                    pinColor="green"
                />
                <Marker
                    coordinate={{ latitude: 23.0830, longitude: 72.5463 }}
                    title={"Chandlodia"}
                    description={"My Living Place"}
                    pinColor="blue"
                />
                <Marker
                    coordinate={{ latitude: 23.0197, longitude: 72.5717 }}>
                    <CustomMarker />
                </Marker>

                <Geojson
                    geojson={myPlace}
                    strokeColor="red"
                    fillColor="green"
                    strokeWidth={2}
                />
               

                {/* <MapViewDirections
                    origin={origin}
                    destination={destination}
                    apikey={GOOGLE_MAPS_APIKEY}
                    strokeWidth={3}
                    strokeColor="hotpink"
                /> */}

                {/* used to drae line on rout point of locations
                < Polyline
                    coordinates={coords}
                    strokeWidth={2}
                />

                start point marker
                <MapView.Marker
                    key={1}
                    coordinate={startMarker}
                />

                end point marker
                <MapView.Marker
                    key={2}
                    coordinate={destMarker}
                >
                </MapView.Marker> */}


            </MapView>

        </View>
    );
};

export default DisplayMapScreen;
