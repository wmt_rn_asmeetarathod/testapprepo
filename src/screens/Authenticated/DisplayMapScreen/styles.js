/* eslint-disable  */
import { StyleSheet } from 'react-native';
import { Color } from 'src/utils';

export const styles = StyleSheet.create({
    scrollViewStyle: {
        flex: 1,
    },
    container: {
        flexGrow: 1,
        alignItems: 'center',
        justifyContent: 'center',
        // backgroundColor: Color.TEXT_SECONDARY,
    },
    vwMap: {
        ...StyleSheet.absoluteFillObject,
        flex:1,
        justifyContent: 'flex-end',
        alignItems: 'center',
    },
    mapStyle: {

        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
    },
    customMarkerStyle: {
        paddingVertical: 10,
        paddingHorizontal: 30,
        backgroundColor: "#007bff",
        borderColor: "#eee",
        borderRadius: 5,
        elevation: 10
    },
    textStyle: {
         color: "#fff",
    }
});
