/* eslint-disable  */
import React, {
    useState,
    useLayoutEffect,
} from 'react';

import { View } from 'react-native';
import { styles } from './styles';
import { Label, Ripple } from 'src/component';
import MapView,
{
    Marker,
    Polygon
} from 'react-native-maps';
import { isPointInPolygon } from 'geolib';

const DrawRouteMapScreen = (props) => {
    const [editing, setEditing] = useState(false);
    const [applyEditing, setApplyEditing] = useState(false);
    const [polygons, setPolygons] = useState([]);

    const [region, setRegion] = useState({
        latitude: 23.0225,
        longitude: 72.5714,
        latitudeDelta: 0.015,
        longitudeDelta: 0.0121,
    });


    const coords = [
        { latitude: 23.0225, longitude: 72.5714 },
        { latitude: 23.0197, longitude: 72.5717 },
        { latitude: 23.0259, longitude: 72.5658 },
        { latitude: 23.016983725799573, longitude: 72.57303582982452 }, // Rf Flower park
        { latitude: 23.01351646334163, longitude: 72.5691876942853 }, // Sanskar Kendra         
        { latitude: 23.0222776533929, longitude: 72.57356241675268 }, //  ellis bridge
        { latitude: 23.01869868281612, longitude: 72.58069159410695 }, // Raikhand
        { latitude: 23.01698372575321, longitude: 72.57996247369573 }, // Gaikwad Haveli
        { latitude: 23.021643884274543, longitude: 72.56047875604001 }, //sanchar colony
        { latitude: 23.018155579847118, longitude: 72.56786520887901 }, // my jio store
        { latitude: 23.016869843343812, longitude: 72.56709447538503 }, //Canada visa application center
        { latitude: 23.01819991536998, longitude: 72.56959935924048 }, // congress bhavan
        { latitude: 23.01491904731148, longitude: 72.56899722369832 }, // kocharab

    ];

    const [markerState, setMarkerState] = useState(coords);

    const onPanDragMap = (e) => {
        e.persist();
        if (editing) {
            if (polygons.length === 0) {
                setPolygons([e.nativeEvent.coordinate]);
            }
            else {
                const newArr = [...polygons, e.nativeEvent.coordinate];
                setPolygons(newArr);
            }

        }

    }

    const applyPolygon = () => {
        setApplyEditing(!applyEditing);

        const tempArr = coords.filter((latLng) => {
            if (isPointInPolygon(latLng, polygons)) {
                return latLng;
            } else {
                return null;
            }
        });

        setMarkerState(tempArr);

    }
    useLayoutEffect(() => {
        props.navigation.setOptions({
            headerRight: () => {
                return (

                    <Ripple
                        rippleContainerBorderRadius={50}
                        onPress={() => applyPolygon()}
                        style={{ marginEnd: 20 }}
                    >
                        <Label>{applyEditing ? 'Apply' : null}</Label>
                    </Ripple>
                )
            },
        })
    }
        , [editing, applyEditing, polygons]);

    const handleDraw = () => {
        setEditing(true);
        setApplyEditing(true);
    }

    const handleClear = () => {
        setEditing(false);
        setApplyEditing(false);
        setMarkerState(coords);
        setPolygons([]);

    }

    return (
        <View style={styles.vwMap}>
            <MapView
                style={styles.mapStyle}
                region={region}
                onPanDrag={(e) => onPanDragMap(e)}
                scrollEnabled={!editing}
            >

                {
                    polygons.length > 0 &&
                    <Polygon
                        coordinates={polygons}
                        strokeColor="#F00"
                        fillColor="rgba(255,0,0,0.5)"
                        strokeWidth={2}
                    />
                }

                {!applyEditing && markerState.map((latLng, index, arr) => {
                    return (<Marker
                        key={index}
                        coordinate={latLng}
                        pinColor="blue"
                    />)
                })
                }
            </MapView>
            {
                editing ?
                    <Label
                        style={styles.lblStyle}
                        mb={10}
                        me={10}
                        normal
                        onPress={() => handleClear()}
                    >
                        {'Clear'}
                    </Label> :
                    <Label
                        style={styles.lblStyle}
                        mb={10}
                        me={10}
                        normal
                        onPress={() => handleDraw()}
                    >
                        {'Draw'}
                    </Label>}
        </View>
    );
};

export default DrawRouteMapScreen;
