/* eslint-disable  */

import {StyleSheet} from 'react-native';
import { Color } from '../../../utils/Color';

export const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    vwMap: {
        ...StyleSheet.absoluteFillObject,
        flex:1,
        justifyContent: 'flex-end',
        alignItems: 'center',
    },
    mapStyle: {

        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
        zIndex:-1,
    },
    lblStyle:{
        color: Color.DARK_LIGHT_BLACK,
        backgroundColor: Color.WHITE,
        alignSelf: 'flex-end',        
        padding : 10,
        borderRadius :10,
    },

});
