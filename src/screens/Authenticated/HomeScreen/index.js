/* eslint-disable  */
import React from 'react';

import {
    View,
    Text,
} from 'react-native';

import { styles } from './styles';

import { Label, Ripple } from 'src/component';
import { CommonStyle } from 'src/utils';
import Routes from 'src/router/Routes';
import Maps from '../DrawRouteMapScreen/maps';


const Home = (props) => {
    /*  Life-cycles Methods */

    /*  Public Interface Methods */

    /*  Validation Methods  */

    /*  UI Events Methods   */

    const goToAddArticleScreen = () => {
        props.navigation.navigate(Routes.AddArticleScreen);
    };

    const goToUserListScreen = () => {
        props.navigation.navigate(Routes.UserListScreen);
    }

    const goToDisplayImageScreen = () =>{
        props.navigation.navigate(Routes.DisplayImageScreen);
    }
    const goToDisplayMapScreen = () =>{
        props.navigation.navigate(Routes.DisplayMapScreen);
    }
    const goToDrawRouteMapScreen = () =>{
        props.navigation.navigate(Routes.DrawRouteMapScreen);
    }
    

    return (
        <View style={CommonStyle.master_full_flex}>
            <View style={styles.container}>
                <Label>{'Home Screen'}</Label>
                <Ripple onPress={goToUserListScreen}>
                    <Label>{'User List'}</Label>
                </Ripple>
                <Ripple onPress={goToAddArticleScreen}>
                    <Label>{'ADD ARTICLE'}</Label>
                </Ripple>
                <Ripple onPress={goToDisplayImageScreen}>
                    <Label>{'IMAGE PICKER DEMO'}</Label>
                </Ripple>
                <Ripple onPress={goToDisplayMapScreen}>
                    <Label>{'GOOGLE MAP DEMO'}</Label>
                </Ripple>
                <Ripple onPress={goToDrawRouteMapScreen}>
                    <Label>{'DRAW ROUTE MAP DEMO'}</Label>
                </Ripple>
            </View>

        </View>
    );
};

export default Home;
