/* eslint-disable  */

import React from 'react';
import {
    View,
    Image,
    Dimensions
} from 'react-native';
import { Label } from 'src/component';
import { styles } from './styles';

const UserDetailsScreen = (props) => {

    const { item } = props.route.params;
    const userName = `${item.name.title} ${item.name.first} ${item.name.last}`;

    return (

        <View style={styles.container}>
            <View style={styles.vwImg}>
                <Image style={styles.imageStyle}
                    source={{ uri: item.picture.large }} />
            </View>
            <View style={styles.vwBottom}>

                <Label large bolder>{userName}</Label>
                <Label normal>{item.email}</Label>
                <Label normal>{item.location.country}</Label>
                <Label normal >{item.phone}</Label>
                <Label normal>{item.dob.date}</Label>
            </View>
        </View>
    );
};

export default UserDetailsScreen;
