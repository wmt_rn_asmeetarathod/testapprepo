/* eslint-disable  */

import {StyleSheet} from 'react-native';
import { Color } from 'src/utils';
import {ThemeUtils} from 'src/utils';

export const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection:'column',
    },
    imageStyle:{
        width: ThemeUtils.relativeWidth(25),
        height: ThemeUtils.relativeWidth(25),
        borderRadius: ThemeUtils.relativeWidth(12.5) ,
        borderWidth : 5,
        borderColor: Color.WHITE,     
    },
    vwImg:{
        display:'flex',        
        justifyContent:'center',
        alignItems:'center',
        backgroundColor: Color.PRIMARY_DARK,
        height: '40%',
        // transform : [ { scaleX : 1  } ],
        // borderBottomStartRadius : 200,
        // borderBottomEndRadius : 200,
        
    },
    vwBottom:{
        padding:20,
    },
    userNameStyle:{
        // fontSize: 19,
        // fontWeight : 'bold',
    },
    textStyle:{
        // fontSize:16,
    },
    
});
