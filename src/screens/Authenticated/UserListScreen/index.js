/* eslint-disable  */

import React, {
    useEffect,
    useState
} from 'react';
import {
    View,
    FlatList,
    Image,
    ActivityIndicator,
} from 'react-native';

import { styles } from './styles';
import {
    Label,
    Ripple
} from 'src/component';
import { CommonStyle } from 'src/utils';
import { APIRequest } from 'src/api';
import Routes from 'src/router/Routes';
import { Color } from 'src/utils';
import { Constants } from 'src/utils';


const UserListScreen = (props) => {

    const [data, setData] = useState([]);
    const [loadingMore, setLoadingMore] = useState(false);
    const [loading, setLoading] = useState(true);

    /*  Life-cycles Methods */
    useEffect(() => {
        fetchUserList();
    }, []);

    const fetchUserList = () => {
        new APIRequest.Builder()
            .get()
            .setReqId(Constants.RequestId)
            .reqURL(Constants.RequestUrl)
            .response(onResponse)
            .error(onError)
            .build()
            .doRequest();
    }

    /*  Public Interface Methods */

    /*  Validation Methods  */


    /*  UI Events Methods   */

    const onResponse = (res, reqId) => {
        setLoading(false);
        let userData = data.length === 0 ? res.data.results : [...data, ...res.data.results];
        setData(userData);

    }

    const onError = () => {
        setLoading(false);
    }

    const handleOnPress = (itemData) => {
        props.navigation.navigate(Routes.UserDetailsScreen, { item: itemData });
    }

    const handleLoadMore = () => {
        setLoadingMore(true);
        fetchUserList();
        setLoadingMore(false);
    }

    /*  Custom-Component sub-render Methods */

    const RenderItemCompo = ({ itemData }) => {
        const userName = `${itemData.name.title} ${itemData.name.first} ${itemData.name.last}`;
        return (
            <Ripple style={styles.itemContainer} onPress={() => handleOnPress(itemData)}>
                <View>
                    <Image style={styles.pictureStyle} source={{ uri: itemData.picture.large }} />
                </View>
                <View style={styles.vwRight}>
                    <Label normal>{userName}</Label>
                    <Label small>{itemData.email}</Label>
                    <Label xsmall >{itemData.location.country}</Label>
                </View>

            </Ripple>
        )

    }

    return (
        <View style={CommonStyle.master_full_flex}>
            {loading ?
                <View style={styles.indicatorView}>
                    <ActivityIndicator size={'large'} color={Color.WHITE} />
                </View>
                :
                <View style={styles.container}>
                    <FlatList
                        data={data}
                        renderItem={({ item }) => (<RenderItemCompo itemData={item} />)}
                        keyExtractor={item => item.login.uuid.toString()}
                        onEndReachedThreshold={0.5}
                        onEndReached={handleLoadMore}
                    />
                    {loadingMore && <ActivityIndicator size={'large'} color={Color.WHITE} />}
                </View>
            }


        </View>
    );
};

export default UserListScreen;
