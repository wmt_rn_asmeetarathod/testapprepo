/* eslint-disable  */
import { StyleSheet } from 'react-native';
import { Color } from 'src/utils';
import { ThemeUtils } from 'src/utils';

export const styles = StyleSheet.create({
    container: {
        display: 'flex',
        backgroundColor: Color.PRIMARY_DARK,
        padding: 10,
        flexDirection: 'column',
        flex: 1,
    },
    itemContainer: {
        display: 'flex',
        flexDirection: 'row',
        margin: 2,
        padding: 7,
        backgroundColor: Color.WHITE,
        borderRadius: 5,
    },
    pictureStyle: {
        height: ThemeUtils.relativeWidth(15),
        width: ThemeUtils.relativeWidth(15),
        borderRadius: ThemeUtils.relativeWidth(7.5),
        margin: 10,
    },
    vwRight: {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',

    },
    indicatorView: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: Color.PRIMARY_DARK,
        flex: 1,
    }
});

