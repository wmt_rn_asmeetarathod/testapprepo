/* eslint-disable  */

import React from 'react';
import { View } from 'react-native';
import { 
    Controller, 
    useForm 
} from 'react-hook-form';
import { CommonActions } from '@react-navigation/routers';

import { 
    MaterialTextInput, 
    RoundButton 
} from 'src/component';
import { CommonStyle, Constants, Messages, ThemeUtils , showToast } from 'src/utils';
import styles from './styles';
import Routes from 'src/router/Routes';

const Login = (props) => {
    /*  Life-cycles Methods */
    const {
        register,
        setValue,
        control,
        reset,
        clearErrors,
        getValues,
        errors,
        handleSubmit,
    } = useForm();

    /*  Public Interface Methods */
    /*  Validation Methods  */

    /*  UI Events Methods   */

    const onSubmit = () => {
        showToast("Login Successfully");
        props.navigation.dispatch(
            CommonActions.reset({
                index: 1,
                routes: [
                    { name: Routes.Authenticated},
                ],
            })
        );
        
    };

    return (
        <View style={CommonStyle.master_full_flex}>
            <View style={styles.container}>
                <Controller
                    control={control}
                    render={({onChange, onBlur, value}) => (
                        <MaterialTextInput
                            onFocus={() => clearErrors('email')}
                            error={errors.email?.message}
                            value={value}
                            label={'Email'}
                            onBlur={onBlur}
                            onChangeText={onChange}
                        />
                )}
                    name={'email'}
                    defaultValue={''}
                    rules={{
                        required: Messages.Errors.emailBlank,
                        pattern: {
                            value: Constants.Regex.PASSWORD,
                            message: Messages.Errors.emailValidity,
                        },
                    }}
                /> 

                <Controller
                    control={control}
                    render={({ onChange, onBlur, value }) => (
                        <MaterialTextInput
                            onFocus={() => clearErrors('password')}
                            error={errors.password?.message}
                            value={value}
                            label={'Password'}
                            secureTextEntry
                            onBlur={onBlur}
                            onChangeText={onChange}
                        />
                    )}
                    name={'password'}
                    defaultValue={''}
                    rules={{ required: 'Password is required.' }}
                />
                
                <RoundButton
                    click={handleSubmit(onSubmit)}
                    width={ThemeUtils.relativeRealWidth(90)}
                    mt={ThemeUtils.relativeRealHeight(4)}
                    // btnPrimary
                    btn_block
                    style={CommonStyle.full_flex}>
                    {'Login'}
                </RoundButton>
                
            </View>
        </View>
    );
};

export default Login;
