/* eslint-disable  */
import {StyleSheet} from 'react-native';
import {ThemeUtils} from 'src/utils';

export default StyleSheet.create({
    container: {
        flex: 1,
        display:'flex',
        marginTop: ThemeUtils.relativeRealHeight(8),
        paddingHorizontal: ThemeUtils.relativeRealWidth(4),
        paddingVertical: ThemeUtils.relativeRealHeight(2),
        // alignItems: 'center',
        justifyContent: 'center',
        // alignContent:'center',
    },
});
